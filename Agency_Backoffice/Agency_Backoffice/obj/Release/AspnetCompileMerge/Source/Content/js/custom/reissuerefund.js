﻿function ReIssueReFund(paxid) {    
    var thisbutton = $("#btnReissue_" + paxid);

    var paxname = $(thisbutton).data("paxname");
    var orderid = $(thisbutton).data("orderid");
    var gdspnr = $(thisbutton).data("gdspnr");
    var paxtype = $(thisbutton).data("paxtype");
    var actiontype = $(thisbutton).data("actiontype");
    var ticketno = $(thisbutton).data("ticketno");
    var pname = $(thisbutton).data("pname");

    $(thisbutton).html("Progress...<i class='fa fa-pulse fa-spinner'></i>");

    $.ajax({
        type: "Post",
        url: "/FlightReport/ReIssueReFund",
        data: '{paxid: ' + JSON.stringify(paxid) + ',paxname: ' + JSON.stringify(paxname) + ',orderid: ' + JSON.stringify(orderid) + ',gdspnr: ' + JSON.stringify(gdspnr) + ',paxtype: ' + JSON.stringify(paxtype) + ',actiontype: ' + JSON.stringify(actiontype) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {                    
                    ShowReIssueReFundPopup(actiontype.toUpperCase(), "#00a300");
                    $("#lblPnr").html(gdspnr);
                    $("#lblTicketNo").html(ticketno);
                    $("#lblPaxName").html(pname);
                }
                else {
                    //ShowMessagePopup("Failed", "#d91717", "error occured.", "#d91717");
                    //setTimeout(function () { }, 5000);
                    ShowMessagePopup(actiontype.toUpperCase(), "#00a300", data[1], "#00a300");
                }
            }
            $(thisbutton).html("Reissue");
        }
    });
}

function ShowReIssueReFundPopup(headerHeading, headcolor) {
    $(".reissuerefundclass").click();
    $(".reissuerefundheading").css("color", headcolor).html(headerHeading);
}

function ShowMessagePopup(headerHeading, headcolor, bodyMsg, bodycolor) {
    $(".thisbuttonmessage").click();
    $(".modelheading").css("color", headcolor).html(headerHeading);
    $(".modelcontent").css("color", bodycolor).html("<p>" + bodyMsg + "</p>");
}