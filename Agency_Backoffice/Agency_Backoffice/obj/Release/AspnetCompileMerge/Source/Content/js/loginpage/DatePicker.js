﻿$('.CommanDate').datepicker({
    dateFormat: "dd/mm/yy",
    showStatus: true,
    showWeeks: true,
    currentText: 'Now',
    autoSize: true,
    maxDate: -0,
    gotoCurrent: true,
    showAnim: 'blind',
    highlightWeek: true

});
$('.offlineDate').datepicker({
    dateFormat: "dd/mm/yy",
    showStatus: true,
    showWeeks: true,
    currentText: 'Now',
    autoSize: true,
    minDate: -0,
    gotoCurrent: true,
    showAnim: 'blind',
    highlightWeek: true

});
$('.CommmanAllDate').datepicker({
    dateFormat: "dd/mm/yy",
    showStatus: true,
    showWeeks: true,
    currentText: 'Now',
    autoSize: true,    
    gotoCurrent: true,
    showAnim: 'blind',
    highlightWeek: true

});
