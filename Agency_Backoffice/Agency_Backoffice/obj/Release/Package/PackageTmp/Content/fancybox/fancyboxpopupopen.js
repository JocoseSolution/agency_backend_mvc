﻿$("a.popupclass").fancybox({
    maxWidth: 1200,
    maxHeight: 800,
    padding: 0,
    fitToView: true,
    copenEffect: 'none',
    closeEffect: 'none',
    prevEffect: 'none',
    nextEffect: 'none',
    closeBtn: true,
    helpers: {
        overlay: {
            locked: false
        }
    }
});

$("a.popupclassreload").fancybox({
    maxWidth: 1200,
    //maxHeight: 800,
    padding: 0,
    fitToView: true,
    copenEffect: 'none',
    closeEffect: 'none',
    prevEffect: 'none',
    nextEffect: 'none',
    closeBtn: true,
    helpers: {
        overlay: {
            locked: false
        }
    }, afterClose: function () { parent.location.reload(!1) }
});
