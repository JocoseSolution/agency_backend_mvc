﻿$('.CommanDate').datepicker({
    dateFormat: "dd/mm/yy",
    showStatus: true,
    showWeeks: true,
    currentText: 'Now',
    autoSize: true,
    maxDate: -0,
    gotoCurrent: true,
    showAnim: 'blind',
    highlightWeek: true

});
$('#txtReturnDate').datepicker({
    dateFormat: "dd/mm/yy",
    showStatus: true,
    showWeeks: true,
    currentText: 'Now',
    autoSize: true,
    minDate: -0,
    gotoCurrent: true,
    showAnim: 'blind',
    highlightWeek: true

});
$('.CommmanAllDate').datepicker({
    dateFormat: "dd/mm/yy",
    showStatus: true,
    showWeeks: true,
    currentText: 'Now',
    autoSize: true,    
    gotoCurrent: true,
    showAnim: 'blind',
    highlightWeek: true

});
$('#txtDepartureDate').datepicker({
    dateFormat: "dd/mm/yy",
    minDate: 0,
    maxDate: null,
    showAnim: "slideDown",
    numberOfMonths: 1,
    changeYear: false,
    changeMonth: false,
    showOtherMonths: true,
    onClose: function () {
        if ($("#txtDepartureDate").val() !== "") {          
            var minDate = $("#txtDepartureDate").datepicker("getDate");
            $('#txtReturnDate').datepicker("option", "minDate", new Date(minDate.getFullYear(), minDate.getMonth(), minDate.getDate()));
            //var toDate = $(".FromDate").datepicker("getDate");
            //$('.ToDate').datepicker("setDate", new Date(toDate.getFullYear(), toDate.getMonth(), toDate.getDate() + 1 ));
        }
    }
});
