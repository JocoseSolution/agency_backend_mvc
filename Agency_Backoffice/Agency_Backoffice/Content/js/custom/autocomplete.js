﻿$("#txtLeavingForm").autocomplete({
    minLength: 3,
    source: function (request, response) {
        $(".setlevfromspinner").removeClass("hidden");
        $.ajax({
            url: ApplicationUrl +"/AutoComplete/GetDepArrCity",
            type: "POST",
            dataType: "json",
            data: { param: request.term },
            success: function (data) {
                response($.map(data, function (item) {                 
                    return { label: item.CityName + " (" + item.AirportCode + ")", value: item.CityName + " (" + item.AirportCode + ")", id: item.CountryCode };
                }))
            }
        })
    },
    select: function (event, ui) {
        if (ui.item.value != "") {
            $(".setlevfromspinner").addClass("hidden");
            $("#hdnFromCountryCode").val(ui.item.id);
        }
    },
    response: function (event, ui) {
        $(".setlevfromspinner").addClass("hidden");
        if (!ui.content.length) {
            var noResult = { value: "", label: "No result found" };
            ui.content.push(noResult);
        }
    },
    messages: {
        noResults: "", results: ""
    }
});

$("#txtLeavingTo").autocomplete({
    minLength: 3,
    source: function (request, response) {
        $(".setlevtospinner").removeClass("hidden");
        $.ajax({
            url: ApplicationUrl +"/AutoComplete/GetDepArrCity",
            type: "POST",
            dataType: "json",
            data: { param: request.term },
            success: function (data) {
                response($.map(data, function (item) {                   
                    return { label: item.CityName + " (" + item.AirportCode + ")", value: item.CityName + " (" + item.AirportCode + ")", id: item.CountryCode };
                }))
            }
        })
    },
    select: function (event, ui) {
        if (ui.item.value != "") {
            $(".setlevtospinner").addClass("hidden");
            $("#hdnToCountryCode").val(ui.item.id);
        }
    },
    response: function (event, ui) {
        $(".setlevtospinner").addClass("hidden");
        if (!ui.content.length) {
            var noResult = { value: "", label: "No result found" };
            ui.content.push(noResult);
        }
    },
    messages: {
        noResults: "", results: ""
    }
});

$("#txtPreferedAirlines").autocomplete({
    minLength: 2,
    source: function (request, response) {
        $(".setpreferedairlinesspinner").removeClass("hidden");
        $.ajax({
            url: ApplicationUrl +"/AutoComplete/GetPreferedAirlines",
            type: "POST",
            dataType: "json",
            data: { param: request.term },
            success: function (data) {
                response($.map(data, function (item) {
                    return { label: item.AL_Name + " (" + item.AL_Code + ")", value: item.AL_Name + " (" + item.AL_Code + ")", id: item.AL_Code };
                }))
            }
        })
    },
    select: function (event, ui) {
        if (ui.item.value != "") { $(".setpreferedairlinesspinner").addClass("hidden"); $("#hdnPAirlinesyCode").val(ui.item.id); }
    },
    response: function (event, ui) {
        $(".setpreferedairlinesspinner").addClass("hidden");
        if (!ui.content.length) {
            var noResult = { value: "", label: "No result found" };
            ui.content.push(noResult);
        }
    },
    messages: {
        noResults: "", results: ""
    }
});