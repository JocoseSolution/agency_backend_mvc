﻿////$("#btnBusSummary").click(function () {
////    $("#btnBusSummaryClick").click();
////});
GetBusSummary = (orderid, tinno) => {
    $("#modelBusHeading").html('<div class="col-sm-12 text-center"><h4>Please wait...<i class="fa fa-spinner fa-pulse"></i></h4></div>');
    $("#BusBodyContent").html('<div class="col-sm-12 text-center"><h4>Please wait...<i class="fa fa-spinner fa-pulse"></i></h4></div>');
    $("#btnBusSummaryClick").click();
    $.ajax({
        type: "Post",
        url: ApplicationUrl + "/Bus/GetBusSelectedSeatDetails",
        data: '{orderid: ' + JSON.stringify(orderid) + ',tinno: ' + JSON.stringify(tinno) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            $("#modelBusHeading").html("").html(data[0]);
            $("#BusBodyContent").html("").html(data[1]);
        }
    });
}