﻿using DataBaseLibrary.Flight;
using ModelLibrary;
using System.Collections.Generic;

namespace Agency_Backoffice.Service
{
    public static class AutoCompleteService
    {
        public static List<CityAutoSearch> CityAutoSearch(string param)
        {
            return FlightLibraryHelper.CityAutoSearch(param);
        }

        public static List<AirLineModel> GetPreferedAirlines(string param)
        {
            return FlightLibraryHelper.GetAirlinesList(param);
        }
    }
}