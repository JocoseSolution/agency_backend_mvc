﻿using DataBaseLibrary.Dashboard;
using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Agency_Backoffice.Service
{
    public static class DashboardService
    {
        public static string GetAirliineSection(string fromDate, string toDate, string agencyId, string salesId)
        {
            string result = string.Empty;
            try
            {
                List<DataTable> objDashList = GetDashboardDetails(fromDate, toDate, agencyId, salesId);
                if (objDashList != null && objDashList.Count > 0)
                {
                    DataTable dtAir = objDashList[0];
                    DataTable dtRail = objDashList[1];
                    DataTable dtAirLine = objDashList[2];
                    DataTable dtPgUpload = objDashList[3];
                    DataTable dtCashInFlow = objDashList[4];
                    DataTable dtCashOutFlow = objDashList[5];
                    DataTable dtRefund = objDashList[6];
                    DataTable dtBus = objDashList[7];
                    DataTable dtHotel = objDashList[8];

                    StringBuilder dbAirLine = new StringBuilder();
                    dbAirLine.Append("<table class='table table-striped jambo_table bulk_action' style='width: 100%!important; '>");
                    dbAirLine.Append("<thead><tr class='headings'><th>Airlines</th><th>AirLine Code</th><th>Gross Sale</th><th>Comm. Passed</th><th>Net Sale</th></tr></thead>");
                    dbAirLine.Append("<tbody>");
                    if (dtAirLine.Rows.Count > 0)
                    {
                        double finalTotalNetSale = 0;
                        double finalTotalSale = 0;
                        double finalCommSale = 0;
                        for (int i = 0; i < dtAirLine.Rows.Count; i++)
                        {
                            dbAirLine.Append("<tr>");
                            dbAirLine.Append("<td>" + (!string.IsNullOrEmpty(dtAirLine.Rows[i]["AirlineName"].ToString()) ? dtAirLine.Rows[i]["AirlineName"].ToString() : "- - -") + "</td>");
                            dbAirLine.Append("<td>" + dtAirLine.Rows[i]["vc"].ToString() + "</td>");
                            dbAirLine.Append("<td>₹ " + dtAirLine.Rows[i]["TotalCost"].ToString() + "</td>");
                            dbAirLine.Append("<td>₹ " + dtAirLine.Rows[i]["TotalComm"].ToString() + "</td>");
                            dbAirLine.Append("<td>₹ " + dtAirLine.Rows[i]["NetSale"].ToString() + "</td>");
                            dbAirLine.Append("</tr>");
                            finalTotalNetSale = finalTotalNetSale + Convert.ToDouble(dtAirLine.Rows[i]["NetSale"].ToString());
                            finalTotalSale = finalTotalSale + Convert.ToDouble(dtAirLine.Rows[i]["TotalCost"].ToString());
                            finalCommSale = finalCommSale + Convert.ToDouble(dtAirLine.Rows[i]["TotalComm"].ToString());
                        }
                        dbAirLine.Append("<tr>");
                        dbAirLine.Append("<td colspan='2' style='text-align: center;'>Total</td><td>₹ " + finalTotalSale + "</td><td>₹ " + finalCommSale + "</td><td>₹ " + finalTotalNetSale + "</td>");
                        dbAirLine.Append("</tr>");
                    }
                    else
                    {
                        dbAirLine.Append("<tr>");
                        dbAirLine.Append("<td colspan='5' style='text-align: center;'>No today sale !</td>");
                        dbAirLine.Append("</tr>");
                    }
                    dbAirLine.Append("</tbody>");
                    dbAirLine.Append("</table>");
                    result = dbAirLine.ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        public static DashbordModel GetDashboardDetail(string fromDate, string toDate, string agencyId, string salesId)
        {
            DashbordModel dashbord = new DashbordModel();
            dashbord.ApplicationUrl = Config.ApplicationUrl;
            try
            {
                List<DataTable> objDashList = GetDashboardDetails(fromDate, toDate, agencyId, salesId);
                if (objDashList != null && objDashList.Count > 0)
                {
                    DataTable dtAir = objDashList[0];
                    DataTable dtRail = objDashList[1];
                    DataTable dtAirLine = objDashList[2];
                    DataTable dtPgUpload = objDashList[3];
                    DataTable dtCashInFlow = objDashList[4];
                    DataTable dtCashOutFlow = objDashList[5];
                    DataTable dtRefund = objDashList[6];
                    DataTable dtBus = objDashList[7];
                    DataTable dtHotel = objDashList[8];

                    dashbord.AirCount = dtAir.Rows[0]["TotalAirCount"].ToString();
                    dashbord.AirAmount = "₹ " + (dtAir.Rows[0]["TotalAirBookingCost"].ToString() != "0" ? dtAir.Rows[0]["TotalAirBookingCost"].ToString() : "0.00");

                    dashbord.RailCount = dtRail.Rows[0]["TotalRailCount"].ToString();
                    dashbord.RailAmount = "₹ " + dtRail.Rows[0]["TotalRailCost"].ToString();

                    dashbord.PgUploadCount = dtPgUpload.Rows[0]["TotalPGUploadCount"].ToString();
                    dashbord.PgUploadAmt = "₹ " + dtPgUpload.Rows[0]["TotalPGUploadAmount"].ToString();

                    dashbord.CashInFlowCount = dtCashInFlow.Rows[0]["TotalCashInFlowCount"].ToString();
                    dashbord.CashInFlowAmt = "₹ " + dtCashInFlow.Rows[0]["TotalCashInFlowAmt"].ToString();

                    dashbord.CashOutFlowCount = dtCashOutFlow.Rows[0]["TotalCashOutFlowCount"].ToString();
                    dashbord.CashOutFlowAmt = "₹ " + dtCashOutFlow.Rows[0]["TotalCashOutFlowAmt"].ToString();

                    dashbord.RefundCount = dtRefund.Rows[0]["TotalRefundCount"].ToString();
                    dashbord.RefundAmt = "₹ " + dtRefund.Rows[0]["TotalRefundAmt"].ToString();

                    dashbord.BusCount = dtBus.Rows[0]["TotalBusCount"].ToString();
                    dashbord.BusAmount = "₹ " + dtBus.Rows[0]["TotalBusAmt"].ToString();

                    dashbord.HotelCount = dtHotel.Rows[0]["TotalHotelCount"].ToString();
                    dashbord.HotelAmt = "₹ " + (dtHotel.Rows[0]["TotalHotelAmt"].ToString() != "0" ? dtHotel.Rows[0]["TotalHotelAmt"].ToString() : "0.00");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return dashbord;
        }
        public static List<DataTable> GetDashboardDetails(string fromDate, string toDate, string agencyId, string salesId)
        {
            return DashboardDatabase.GetDashboardDetail(fromDate, toDate, agencyId, salesId);
        }
    }
}