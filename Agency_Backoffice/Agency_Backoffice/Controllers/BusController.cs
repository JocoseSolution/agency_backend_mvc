﻿using Agency_Backoffice.Models;
using Agency_Backoffice.Models.Common;
using Agency_Backoffice.Service;
using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Agency_Backoffice.Controllers
{
    public class BusController : Controller
    {
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult BusReport(string btnvalue, BusFilter filter)
        {
            BusModel model = new BusModel();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    if (btnvalue == "search")
                    {
                        UpdateModel(model);
                    }
                    filter.FilterLoginId = filter.FilterAgentId = lu.AgentRegisterSession.UID;
                    model = BusService.GetBusReport(filter);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult BusRefundReport(string btnvalue, BusFilter filter)
        {
            BusRefund model = new BusRefund();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    if (btnvalue == "search")
                    {
                        UpdateModel(filter);
                    }
                    filter.FilterLoginId = filter.FilterAgentId = lu.AgentRegisterSession.UID;
                    model.BusRefundList = BusService.GetBusRefundReport(filter);
                    model.TotalCount = model.BusRefundList.Count;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult BusRejectReport(string btnvalue, BusFilter filter)
        {
            BusReject model = new BusReject();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    if (btnvalue == "search")
                    {
                        UpdateModel(filter);
                    }
                    filter.FilterLoginId = filter.FilterAgentId = lu.AgentRegisterSession.UID;
                    model.BusRejectList = BusService.GetBusRejectReport(filter);
                    model.TotalCount = model.BusRejectList.Count;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult BusHoldPnrReport(string btnvalue, BusFilter filter)
        {
            BusHoldPnr model = new BusHoldPnr();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    if (btnvalue == "search")
                    {
                        UpdateModel(filter);
                    }
                    filter.FilterLoginId = filter.FilterAgentId = lu.AgentRegisterSession.UID;
                    model.BusHoldPnrList = BusService.GetBusHoldPnrReport(filter);
                    model.TotalCount = model.BusHoldPnrList.Count;
                    model.ExecutiveList = Commoncls.CommonPopulateSelectListItem(BusService.ExecutiveList());
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }
        public JsonResult GetBusSelectedSeatDetails(string orderid, string tinno)
        {
            List<string> result = new List<string>();
            LoginSessionDetails lu = new LoginSessionDetails();
            if (AccountService.GetLoginSession(ref lu))
            {
                result = BusService.GetBusSelectedSeatDetails(orderid, lu.AgentRegisterSession.AgencyId, tinno);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}