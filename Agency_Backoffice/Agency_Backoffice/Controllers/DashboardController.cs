﻿using Agency_Backoffice.Models;
using Agency_Backoffice.Service;
using System.Web.Mvc;

namespace Agency_Backoffice.Controllers
{
    public class DashboardController : Controller
    {
        public ActionResult Dashboard()
        {
            string salesId = "";
            string fromDate = "";
            string toDate = "";
            string agencyId = "";
            LoginSessionDetails lu = new LoginSessionDetails();
            if (AccountService.GetLoginSession(ref lu))
            {
                agencyId = lu.AgentRegisterSession.UID;
            }
            return View(DashboardService.GetDashboardDetail(fromDate, toDate, agencyId, salesId));
        }
        public JsonResult AirliineSection()
        {
            string salesId = "";
            string fromDate = "";
            string toDate = "";
            string agencyId = "";
            LoginSessionDetails lu = new LoginSessionDetails();
            if (AccountService.GetLoginSession(ref lu))
            {
                agencyId = lu.AgentRegisterSession.UID;
            }
            return Json(DashboardService.GetAirliineSection(fromDate, toDate, agencyId, salesId));
        }
    }
}