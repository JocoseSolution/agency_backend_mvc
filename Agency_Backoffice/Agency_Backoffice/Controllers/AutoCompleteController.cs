﻿using Agency_Backoffice.Service;
using System.Linq;
using System.Web.Mvc;

namespace Agency_Backoffice.Controllers
{
    public class AutoCompleteController : Controller
    {
        public JsonResult GetDepArrCity(string param)
        {
            var cityList = AutoCompleteService.CityAutoSearch(param).Take(10).ToList();
            return Json(cityList);
        }

        public JsonResult GetPreferedAirlines(string param)
        {
            var airlinesList = AutoCompleteService.GetPreferedAirlines(param).Take(10).ToList();
            return Json(airlinesList);
        }
    }
}