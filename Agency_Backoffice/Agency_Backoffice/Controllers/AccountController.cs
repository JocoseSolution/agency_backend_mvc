﻿using Agency_Backoffice.Models;
using Agency_Backoffice.Models.Common;
using Agency_Backoffice.Service;
using ModelLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace Agency_Backoffice.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult Index(string userid, string password, string returnurl)
        {
            if (!string.IsNullOrEmpty(userid) && !string.IsNullOrEmpty(password))
            {
                password = GetAgencyPassword(userid, password);                

                List<string> result = CheckUserDetails(userid, password);
                if (result != null && result.Count > 0)
                {
                    if (result[0] == "true")
                    {
                        if (returnurl != null && !string.IsNullOrEmpty(returnurl.Trim()))
                        {
                            return Redirect(returnurl);
                        }
                        else
                        {
                            return Redirect(Config.ApplicationUrl + result[1]);
                        }
                    }
                }
            }
            return View();
        }
        public ActionResult ForgotPassword()
        {
            return View();
        }
        public ActionResult LedgerReport(string bookingtype, string type, AccountLedgerFilter filter)
        {
            AccountLedgerReport model = new AccountLedgerReport();
            try
            {
                model.Passbook = type;
                AccountLedgerFilter tempfilter = GetPrevFilter(bookingtype, filter);
                filter = tempfilter;
                if (!string.IsNullOrEmpty(bookingtype))
                {
                    filter.BookingType = GetBookingType(bookingtype);
                }

                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    filter.UserType = lu.AgentRegisterSession.User_Type;
                    filter.Loginid = lu.AgentRegisterSession.UID;
                }
                // model = AccountService.GetClosingbal(filter);
                model.AccountLedgerReportList = AccountService.GetLedgerDetail(filter);
                model.Totalcount = model.AccountLedgerReportList.Count;

                if (!string.IsNullOrEmpty(tempfilter.FromDate))
                {
                    model.FromDate = tempfilter.FromDate;
                }
                if (!string.IsNullOrEmpty(tempfilter.ToDate))
                {
                    model.ToDate = tempfilter.ToDate;
                }
                if (!string.IsNullOrEmpty(tempfilter.Price))
                {
                    model.Price = tempfilter.Price;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public ActionResult IntInvoiceDetails(string orderid)
        {
            AccountLedgerReport model = new AccountLedgerReport();
            try
            {
                model = AccountService.GetInvoice(orderid);
                model.DepDate = Commoncls.GetSpilitedDateFormate(model.DepDate);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }
        public ActionResult StaffTransaction(AccountLedgerFilter filter)
        {
            AccountLedgerReport model = new AccountLedgerReport();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    filter.UserType = lu.AgentRegisterSession.User_Type;
                    filter.Loginid = lu.AgentRegisterSession.UID;
                    filter.SearchType = "GRIDBIND";
                }

                model.AccountLedgerReportList = AccountService.GetStaffTransaction(filter);
                model.Totalcount = model.AccountLedgerReportList.Count;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public ActionResult DomSaleRegister(AccountLedgerFilter filter)
        {
            AccountLedgerReport model = new AccountLedgerReport();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    filter.UserType = lu.AgentRegisterSession.User_Type;
                    filter.Loginid = lu.AgentRegisterSession.UID;
                    filter.Trip = "D";
                }

                model.AccountLedgerReportList = AccountService.IntGetInvoice(filter);
                model.Totalcount = model.AccountLedgerReportList.Count;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public ActionResult IntlSaleRegister(AccountLedgerFilter filter)
        {
            AccountLedgerReport model = new AccountLedgerReport();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    filter.UserType = lu.AgentRegisterSession.User_Type;
                    filter.Loginid = lu.AgentRegisterSession.UID;
                    filter.Trip = "I";
                }

                model.AccountLedgerReportList = AccountService.IntGetInvoice(filter);
                model.Totalcount = model.AccountLedgerReportList.Count;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public ActionResult AgentProfile()
        {
            AgentProfileModel model = new AgentProfileModel();

            try
            {
                AgencyLoginSession lu = new AgencyLoginSession();
                if (!AccountService.IsAgencyLogin(ref lu))
                {
                    if (AccountService.LogoutBackendLogin())
                    {
                        Response.Redirect(Config.WebsiteLoginUrl);
                    }
                }
                else
                {
                    //if (System.IO.File.Exists(Server.MapPath(Utility.GetFullPathWithCustom("CompanyLogo", lu.User_Id) + (lu.User_Id + ".jpeg"))))
                    //{
                    if (System.IO.File.Exists(Server.MapPath("/Content/images/CompanyLogo/" + lu.User_Id + "/" + (lu.User_Id + ".jpeg"))))
                    {
                        model.Image_Url = "/Content/images/CompanyLogo/" + lu.User_Id + "/" + (lu.User_Id + ".jpeg");
                    }
                    else
                    {
                        model.Image_Url = Config.ApplicationUrl + "/Content/images/logonotfound.png";
                    }

                    model.Mobile = lu.Mobile;
                    model.Email = lu.Email;
                    model.FName = lu.FName;
                    model.LName = lu.LName;
                    model.Password = lu.Password;
                    model.Agency_Name = lu.Agency_Name;
                    model.Address = lu.Address;
                    model.User_Id = lu.User_Id;
                    model.Is_GST_Apply = lu.Is_GST_Apply;
                    model.GST_Remark = lu.GST_Remark;
                    model.GST_Number = lu.GSTNo;
                    model.Gst_CompanyName = lu.GST_Company_Name;
                    model.Gst_CompAddress = lu.GST_Company_Address;
                    model.Gst_State = lu.GST_State;
                    model.Gst_City = lu.GST_City;
                    model.Gst_Pincode = lu.GST_Pincode;
                    model.Gst_Phone = lu.GST_PhoneNo;
                    model.Gst_Email = lu.GST_Email;
                    model.StateList = Commoncls.PopulateState(AccountService.StateList(lu.Country));
                    model.CityList = Commoncls.PopulateCity(AccountService.CityList(lu.GST_StateCode));
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }
        private string GetBookingType(string btype)
        {
            if (!string.IsNullOrEmpty(btype))
            {
                if (btype.ToLower().Trim() == "flight")
                {
                    return "dom";
                }
                else if (btype.ToLower().Trim() == "hotel")
                {
                    return "htl";
                }
                else if (btype.ToLower().Trim() == "bus")
                {
                    return "bus";
                }
                else if (btype.ToLower().Trim() == "rail")
                {
                    return "rail";
                }
            }
            return string.Empty;
        }
        private AccountLedgerFilter GetPrevFilter(string bookingtype, AccountLedgerFilter filter)
        {
            AccountLedgerFilter prevfilter = new AccountLedgerFilter();

            if (TempData["filter"] != null && !string.IsNullOrEmpty(bookingtype))
            {
                prevfilter = TempData["filter"] as AccountLedgerFilter;
            }

            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                prevfilter.FromDate = filter.FromDate;
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                prevfilter.ToDate = filter.ToDate;
            }
            if (!string.IsNullOrEmpty(filter.Price))
            {
                prevfilter.Price = filter.Price;
            }
            if (!string.IsNullOrEmpty(filter.OrderId))
            {
                prevfilter.OrderId = filter.OrderId;
            }
            if (!string.IsNullOrEmpty(filter.AirLine))
            {
                prevfilter.AirLine = filter.AirLine;
            }

            TempData["filter"] = prevfilter;
            TempData.Keep();
            return prevfilter;
        }

        #region [Json Section]
        public JsonResult ProcessBackendLogin(string username, string password)
        {
            return Json(CheckUserDetails(username, password));
        }
        private List<string> CheckUserDetails(string username, string password)
        {
            List<string> result = new List<string>();

            try
            {
                string msg = string.Empty;
                string redirectUrl = string.Empty;

                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                {
                    AgentStaffLogin login = new AgentStaffLogin();
                    login.UserId = username;
                    login.Password = password;
                    login.IpAddress = "";

                    bool isSuccessLogin = AccountService.LoginProcess(login, ref msg, ref redirectUrl);
                    if (isSuccessLogin)
                    {
                        result.Add("true");
                        result.Add(redirectUrl);
                    }
                    else
                    {
                        result.Add("false");
                        result.Add(msg);
                    }
                }
                else
                {
                    result.Add("false");
                    result.Add("Please enter userid and password !");
                }
            }
            catch (Exception ex)
            {
                result.Add("false");
                result.Add(ex.ToString());
            }

            return result;
        }
        public JsonResult ProcessForgetPassword(string userid, string email, string mobile)
        {
            List<string> result = new List<string>();

            try
            {
                string msg = string.Empty;

                if (!string.IsNullOrEmpty(userid) && !string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(mobile))
                {
                    ForgetPassword forget = new ForgetPassword();
                    forget.UserId = userid;
                    forget.Email = email;
                    forget.Mobile = mobile;

                    msg = AccountService.ForgetPassword(forget);
                    if (msg.Trim().ToLower().Contains("invalid") || msg.Trim().ToLower().Contains("failed"))
                    {
                        result.Add("false");
                        result.Add(msg);
                    }
                    else
                    {
                        result.Add("true");
                        result.Add(msg);
                    }
                }
                else
                {
                    result.Add("false");
                    result.Add("Please enter userid, email and mobile !");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult ProcessToChangePassword(string password)
        {
            List<string> result = new List<string>();
            AgencyLoginSession lu = new AgencyLoginSession();

            try
            {
                if (!string.IsNullOrEmpty(password))
                {
                    if (!AccountService.IsAgencyLogin(ref lu))
                    {
                        if (AccountService.LogoutBackendLogin())
                        {
                            Response.Redirect("/");
                        }
                    }
                    else
                    {
                        AgentUpdateProfile profile = new AgentUpdateProfile();
                        profile.Type = "Login";
                        profile.AgentId = lu.User_Id;
                        profile.Password = password;
                        profile.AgentEmail = "";
                        profile.LandLine = "";
                        profile.Fax = "";
                        profile.Address = lu.Address;
                        profile.City = lu.City;
                        profile.State = lu.State;
                        profile.StateCode = lu.StateCode;
                        profile.Country = lu.Country;
                        profile.GSTNO = "";
                        profile.GSTCompanyName = "";
                        profile.GSTCompanyAddress = "";
                        profile.GSTPhoneNo = "";
                        profile.GSTEmail = "";
                        profile.IsGSTApply = true;
                        profile.GSTRemark = "";
                        profile.GstPinCode = "";

                        if (AccountService.UpdateAgentProfileDetails(profile))
                        {
                            result.Add("success");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Add("error");
                result.Add(ex.Message);
            }

            return Json(result);
        }
        public JsonResult ProcessToChangeCompanyLogo()
        {
            List<string> result = new List<string>();
            AgencyLoginSession lu = new AgencyLoginSession();

            try
            {
                if (!AccountService.IsAgencyLogin(ref lu))
                {
                    if (AccountService.LogoutBackendLogin())
                    {
                        Response.Redirect("/");
                    }
                }
                else
                {
                    if (Request.Files.Count > 0)
                    {
                        HttpFileCollectionBase files = Request.Files;
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFileBase file = files[i];
                            string fname = file.FileName;

                            if (!string.IsNullOrEmpty(fname))
                            {
                                #region [Backoffice]
                                Directory.CreateDirectory(Server.MapPath(Utility.GetFullPathWithCustom("CompanyLogo", lu.User_Id)));
                                string imageurlname = lu.User_Id + ".jpg";
                                string filePath = Server.MapPath(Utility.GetFullPathWithCustom("CompanyLogo", lu.User_Id) + imageurlname);
                                if (System.IO.File.Exists(filePath))
                                {
                                    System.IO.File.Delete(filePath);
                                }

                                file.SaveAs(filePath);
                                #endregion

                                #region [Front Web]
                                //string frontwebpath = "E:\\Software\\QuickStartFile\\nolonger\\Bitbucket\\tripmaza_front_newproject\\AgentLogo\\" + imageurlname;
                                string frontwebpath = "C:\\WEBAPP\\Web_MVC\\FrontWeb\\front_newproject\\AgentLogo\\" + imageurlname;
                                if (System.IO.File.Exists(frontwebpath))
                                {
                                    System.IO.File.Delete(frontwebpath);
                                }

                                file.SaveAs(frontwebpath);
                                #endregion
                                result.Add("success");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Add("error");
                result.Add(ex.Message);
            }

            return Json(result);
        }
        public JsonResult ProcessToUpdateGstDetail(AgentProfileModel gstdel)
        {
            List<string> result = new List<string>();
            AgencyLoginSession lu = new AgencyLoginSession();

            try
            {
                if (!AccountService.IsAgencyLogin(ref lu))
                {
                    if (AccountService.LogoutBackendLogin())
                    {
                        Response.Redirect("/");
                    }
                }
                else
                {
                    AgentUpdateProfile profile = new AgentUpdateProfile();
                    profile.Type = "GST";
                    profile.AgentId = lu.User_Id;
                    profile.Password = "";
                    profile.AgentEmail = "";
                    profile.LandLine = "";
                    profile.Fax = "";
                    profile.Address = lu.Address;
                    profile.City = gstdel.Gst_City;
                    profile.State = gstdel.Gst_State;
                    profile.StateCode = gstdel.Gst_StateCode;
                    profile.Country = lu.Country;
                    profile.GSTNO = gstdel.GST_Number;
                    profile.GSTCompanyName = gstdel.Gst_CompanyName;
                    profile.GSTCompanyAddress = gstdel.Gst_CompAddress;
                    profile.GSTPhoneNo = gstdel.Gst_Phone;
                    profile.GSTEmail = gstdel.Gst_Email;
                    profile.IsGSTApply = gstdel.Is_GST_Apply.ToLower().Trim() == "true" ? true : false;
                    profile.GSTRemark = gstdel.GST_Remark;
                    profile.GstPinCode = gstdel.Gst_Pincode;

                    if (AccountService.UpdateAgentProfileDetails(profile))
                    {
                        AccountService.ReBindAgencyDetails(lu.User_Id);
                        result.Add("success");
                    }
                }
            }
            catch (Exception ex)
            {
                result.Add("error");
                result.Add(ex.Message);
            }
            return Json(result);
        }
        public JsonResult BindCityList(string stateID)
        {
            List<SelectListItem> cityList = Commoncls.PopulateCity(AccountService.CityList(stateID));
            return Json(cityList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AgencyLogout()
        {
            List<string> result = new List<string>();
            if (AccountService.LogoutBackendLogin())
            {
                result.Add("success");
                result.Add(Config.WebsiteSearchUrl + "?logout=true");
            }
            return Json(result);
        }
        private string GetAgencyPassword(string userid, string password)
        {
            string rutPassword = string.Empty;
            if (!string.IsNullOrEmpty(userid))
            {
                AgencyDetails agencyDel = AccountService.GetAgencyDetails(userid);
                if (agencyDel != null)
                {
                    //if (agencyDel.Password.Contains(password))
                    //{
                    rutPassword = agencyDel.Password;
                    //}
                }
            }
            return rutPassword;
        }
        #endregion
    }
}