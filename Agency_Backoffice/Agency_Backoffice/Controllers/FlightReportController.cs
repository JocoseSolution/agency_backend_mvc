﻿using Agency_Backoffice.Models;
using Agency_Backoffice.Models.Common;
using Agency_Backoffice.Service;
using ClosedXML.Excel;
using DataBaseLibrary.Flight;
using DataBaseLibrary.MailingCredentiaL;
using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using UtilityLibrary;

namespace Agency_Backoffice.Controllers
{
    public class FlightReportController : Controller
    {
        public ActionResult TicketReport(FlightTicketFilter filter)
        {
            FlightTicketReport model = new FlightTicketReport();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    filter.UserType = lu.AgentRegisterSession.User_Type;
                    filter.Loginid = lu.AgentRegisterSession.UID;
                    filter.Status = FlightStatus.Ticketed.ToString();                    
                }

                model.FlightTicketReportList = FlightReportService.GetFlightTicketReports(filter);
                model.Totalcount = model.FlightTicketReportList.Count;

                TempData["FlightFilter"] = filter;
                TempData.Keep();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public ActionResult TicketDetailsReport(string orderId, string transId)
        {
            SingleTicketReportModel model = new SingleTicketReportModel();
            try
            {
                model.BookingDetail = FlightReportService.GetBookingDetails(orderId);
                model.PaymentDetail = FlightReportService.GetPayementProcess(orderId);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public JsonResult GetTicketDetails(string orderid)
        {
            return Json(FlightReportService.GetTicketDetails(orderid), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Invoice(string orderId)
        {
            FlightInvoice model = new FlightInvoice();
            try
            {
                //    model.FlightInvoiceList = FlightReportService.GetInvoiceDetails(orderId);
                //    model.PaxList = FlightReportService.SelectPaxDetail(orderId, "");
                //    model.FltDetailsList = FlightReportService.SelectFlightDetail(orderId);
                //    model.FlightInvoiceList[0].DepartureDate = Commoncls.GetSpilitedDateFormate(model.FlightInvoiceList[0].DepartureDate);
                //    model.AgencyList = FlightReportService.GetAgencyDetailByAgentId(model.FlightInvoiceList[0].AgentId);
                model.OrderId = orderId;
                model.Invoicecopy = FlightDataBase.InvoiceCopy(orderId);

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }
        public ActionResult ReIssueReFund(ReIssueReFund model, string paxid, string paxname, string orderid, string gdspnr, string paxtype, string actiontype)
        {
            try
            {
                if (!string.IsNullOrEmpty(paxid) && !string.IsNullOrEmpty(actiontype))
                {
                    if (actiontype == "reissue")
                    {
                        string firstStatus = FlightReportService.CheckTktNo(Convert.ToInt32(paxid), orderid, gdspnr);

                        model = FlightReportService.GetTicketdIntl(Convert.ToInt32(paxid), paxtype);

                        string newpaxid = paxid;

                        if (!string.IsNullOrEmpty(model.ResuID))
                        {
                            ReIssueReFund Paxds = FlightReportService.OldPaxInfo(model.ResuID, model.Title, model.FName, model.MName, model.LName, model.PaxType);
                            //newpaxid = Paxds.PaxId.ToString();
                        }

                        string secondStatus = FlightReportService.CheckTktNo(Convert.ToInt32(newpaxid.Trim()), model.OrderId, model.PNR);

                        if (secondStatus == "0")
                        {
                        }
                        else if (secondStatus == "Reissue request can not be accepted for past departure date." && actiontype == "refund")
                        {
                        }
                        else if ((secondStatus == "0" || secondStatus == "Reissue request can not be accepted for past departure date." || secondStatus == "Given ticket number is already ReIssued") || firstStatus == "Refund/Reissue request can not be accepted upto 4 hour prior to departure date. Please contact to airline directly." && actiontype == "refund")
                        {

                        }
                        else if (firstStatus == "Refund/Reissue request can not be accepted upto 4 hour prior to departure date. Please contact to airline directly." && actiontype == "refund")
                        {
                        }
                        else
                        {
                            TempData["Message"] = secondStatus;
                        }
                    }
                    else
                    {
                        string firstStatus = FlightReportService.CheckTktNo(Convert.ToInt32(paxid), orderid, gdspnr);

                        model = FlightReportService.GetTicketdIntl(Convert.ToInt32(paxid), paxtype);

                        string newpaxid = paxid;

                        if (!string.IsNullOrEmpty(model.ResuID))
                        {
                            ReIssueReFund Paxds = FlightReportService.OldPaxInfo(model.ResuID, model.Title, model.FName, model.MName, model.LName, model.PaxType);
                            //newpaxid = Paxds.PaxId.ToString();
                        }

                        string secondStatus = FlightReportService.CheckTktNo(Convert.ToInt32(newpaxid.Trim()), model.OrderId, model.PNR);

                        if (secondStatus == "0")
                        {
                        }
                        else if (secondStatus == "Reissue request can not be accepted for past departure date." && actiontype == "refund")
                        {
                        }
                        else if ((secondStatus == "0" || secondStatus == "Reissue request can not be accepted for past departure date." || secondStatus == "Given ticket number is already ReIssued") || firstStatus == "Refund/Reissue request can not be accepted upto 4 hour prior to departure date. Please contact to airline directly." && actiontype == "refund")
                        {

                        }
                        else if (firstStatus == "Refund/Reissue request can not be accepted upto 4 hour prior to departure date. Please contact to airline directly." && actiontype == "refund")
                        {
                        }
                        else
                        {
                            TempData["Message"] = secondStatus;
                        }
                    }

                    model.ActionType = actiontype.ToUpper();
                }

                model.PaxName = paxname;
            }
            catch (Exception ex)
            {
                ex.ToString();
                //clsErrorLog.LogInfo(ex)
            }

            return View(model);
        }
        [HttpPost]
        public ActionResult ReIssueReFund(ReIssueReFund model, string actiontype)
        {
            try
            {
                if (model.ActionType.ToLower() == "refund")
                {

                }
                else
                {
                    if (model.ActionType.ToLower() == "reissue")
                    {
                        ReIssueReFund fltds = FlightReportService.GetTicketdIntl(Convert.ToInt32(model.PaxID), model.PaxType);
                        if (fltds != null && !string.IsNullOrEmpty(fltds.UserID))
                        {
                            string status = FlightReportService.CheckTktNo(Convert.ToInt32(model.PaxID), model.OrderId, model.PNR);

                            if (status == "0")
                            {
                                fltds.Remark = model.Remark;
                                fltds.PaxID = model.PaxID;
                                fltds.ProjectID = !string.IsNullOrEmpty(model.ProjectID) ? model.ProjectID : "Nothing";
                                fltds.BillNoAir = !string.IsNullOrEmpty(model.BillNoAir) ? model.BillNoAir : "Nothing";
                                fltds.BillNoCorp = "Nothing";
                                fltds.ReqTyp = "R";
                                fltds.Status = FlightStatus.Pending.ToString();
                                fltds.RefundID = UtilityModel.GenrateRandomTransactionId("RISU", 10, "1234567890");

                                if (model.ActionType.ToLower() == "reissue")
                                {
                                    bool isInsert = FlightReportService.InsertReIssueCancelIntl(fltds);

                                    LoginSessionDetails lu = new LoginSessionDetails();
                                    if (AccountService.GetLoginSession(ref lu))
                                    {
                                        if (!string.IsNullOrEmpty(lu.AgentRegisterSession.LoginByOTP) && lu.AgentRegisterSession.LoginByOTP.ToLower().Trim() == "true")
                                        {
                                            string UserId = lu.AgentRegisterSession.UID;
                                            string Remark = model.Remark;
                                            string OTPRefNo = fltds.OrderId;
                                            string LoginByOTP = lu.AgentRegisterSession.LoginByOTP;
                                            string OTPId = "Nothing"; //Not Clear
                                            string ServiceType = "FLIGHT-TICKET" + model.ActionType.ToLower().Trim();
                                            bool flag = FlightReportService.OTPTransactionInsert(UserId, Remark, OTPRefNo, LoginByOTP, OTPId, ServiceType);
                                        }

                                        Dictionary<string, string> dicEmailCrets = FlightReportService.GetEmail_Credentilas(fltds.OrderId, "ReIssue_REJECTED", model.PaxID);

                                        MailingCredential mailCred = MailingCredentialLibraryHelper.GetMailingDetails(enumMAILING.AIR_PNRSUMMARY.ToString(), lu.AgentRegisterSession.UID);

                                        AgencyLoginSession agency = new AgencyLoginSession();
                                        if (AccountService.IsAgencyLogin(ref agency))
                                        {
                                            if (mailCred != null && mailCred.Counter > 0)
                                            {
                                                string strBody = EmailFormate.EmailFormateReissueRequest(fltds);

                                                bool isMailSent = Mail.SendMail(agency.Email, mailCred.MailFrom, mailCred.BCC, mailCred.CC, mailCred.SmtpClient, mailCred.UserID, mailCred.Pass, strBody, "Ticket Reissue Request");
                                            }
                                        }
                                    }

                                    if (isInsert)
                                    {
                                        TempData["Message"] = "Remark Submitted Succesfully";
                                    }
                                }
                            }
                            else
                            {
                                //TempData["Message"] = secondStatus;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public ActionResult RefundReport(FlightTicketFilter filter)
        {
            FlightTicketReport model = new FlightTicketReport();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    filter.UserType = lu.AgentRegisterSession.User_Type;
                    filter.Loginid = lu.AgentRegisterSession.UID;
                    filter.Status = FlightStatus.Cancelled.ToString();
                    TempData["RefundFilter"] = filter;
                    TempData.Keep();
                }

                model.FlightTicketReportList = FlightReportService.GetFlightRefundReports(filter);
                model.Totalcount = model.FlightTicketReportList.Count;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public ActionResult ReissuesReport(FlightTicketFilter filter)
        {
            FlightTicketReport model = new FlightTicketReport();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    filter.UserType = lu.AgentRegisterSession.User_Type;
                    filter.Loginid = lu.AgentRegisterSession.UID;
                    TempData["ReissuesFilter"] = filter;
                    TempData.Keep();
                }

                model.FlightTicketReportList = FlightReportService.GetReIssueDetail(filter);
                model.Totalcount = model.FlightTicketReportList.Count;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public ActionResult TicketStatusReport(FlightTicketFilter filter)
        {
            FlightTicketReport model = new FlightTicketReport();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    filter.UserType = lu.AgentRegisterSession.User_Type;
                    filter.Loginid = lu.AgentRegisterSession.UID;
                }

                model.FlightTicketReportList = FlightReportService.GetTicketStatusReport(filter);
                model.Totalcount = model.FlightTicketReportList.Count;

                TempData["TicketStatusFilter"] = filter;
                TempData.Keep();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public ActionResult HoldPNRReport(FlightTicketFilter filter)
        {
            FlightTicketReport model = new FlightTicketReport();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    filter.UserType = lu.AgentRegisterSession.User_Type;
                    filter.Loginid = lu.AgentRegisterSession.UID;
                    filter.Actionby = "REPORT";                    
                }

                model.FlightTicketReportList = FlightReportService.GetHoldPNRReport(filter);
                model.Totalcount = model.FlightTicketReportList.Count;

                TempData["HoldPnrFilter"] = filter;
                TempData.Keep();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public ActionResult OfflineRequest()
        {
            return View();
        }
        public ActionResult FlightPassengerDetail(string proxyid)
        {
            FlightPassengerDetail model = new FlightPassengerDetail();
            try
            {
                model = FlightReportService.GetFlightPassengerDetail(proxyid);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }
        public ActionResult OfflineRequestList(FlightTicketFilter filter)
        {
            OfflineRequest model = new OfflineRequest();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    filter.UserType = lu.AgentRegisterSession.User_Type;
                    filter.Loginid = lu.AgentRegisterSession.UID;
                }

                model.OfflineRequestList = FlightReportService.GetProxyBookingReport(filter);
                model.Totalcount = model.OfflineRequestList.Count;

                TempData["OfflineRequestFilter"] = filter;
                TempData.Keep();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }

        #region [Travel Calender]
        public ActionResult FlightTravelCalender()
        {
            return View();
        }
        public JsonResult GetFlightTravDetail(string currmonth, string today)
        {
            FlightCalender fltCal = new FlightCalender();

            LoginSessionDetails lu = new LoginSessionDetails();
            if (AccountService.GetLoginSession(ref lu))
            {
                fltCal.UserType = lu.AgentRegisterSession.User_Type;
                fltCal.LoginId = lu.AgentRegisterSession.UID;

                string[] datesplit = today.Split('-');
                if (datesplit[0].Length == 1)
                {
                    datesplit[0] = "0" + datesplit[0];
                }
                if (datesplit[1].Length == 1)
                {
                    datesplit[1] = "0" + datesplit[1];
                }
                fltCal.Date = datesplit[0] + datesplit[1] + datesplit[2].Replace("20", "");
            }
            List<FlightTravCalender> result = FlightReportService.GetFlightDetailsByAgentId_New(fltCal);
            return Json(result);
        }
        public JsonResult GetFlightTravIntlDetail(string today, FlightCalender model)
        {
            FlightCalender fltCal = new FlightCalender();
            DateTime DTSearch = new DateTime();

            LoginSessionDetails lu = new LoginSessionDetails();
            if (AccountService.GetLoginSession(ref lu))
            {
                fltCal.UserType = lu.AgentRegisterSession.User_Type;
                fltCal.LoginId = lu.AgentRegisterSession.UID;

                string[] datesplit = today.Split('-');
                if (datesplit[0].Length == 1)
                {
                    datesplit[0] = "0" + datesplit[0];
                }
                if (datesplit[1].Length == 1)
                {
                    datesplit[1] = "0" + datesplit[1];
                }
                fltCal.Date = datesplit[0] + datesplit[1] + datesplit[2].Replace("20", "");
                DTSearch = Convert.ToDateTime(datesplit[2] + "/" + datesplit[1] + "/" + datesplit[0]);

                fltCal.PNR = model.PNR;
                fltCal.AirlineCode = model.AirlineCode;
                fltCal.PaxName = model.PaxName;
                fltCal.Source = model.Source;
                fltCal.Destination = model.Destination;
                fltCal.Trip = model.Trip;
                fltCal.TripType = model.TripType;
            }
            List<string> result = new List<string>();
            result.Add(DTSearch.ToString("dddd, dd MMMM yyyy"));
            result.Add(FlightReportService.GetTicketDetail_Intl_calender(fltCal));
            return Json(result);
        }
        public JsonResult GetFlightTravIntlDetailbySearch(FlightCalender model)
        {
            LoginSessionDetails lu = new LoginSessionDetails();
            if (AccountService.GetLoginSession(ref lu))
            {
                model.UserType = lu.AgentRegisterSession.User_Type;
                model.LoginId = lu.AgentRegisterSession.UID;

                string[] datesplit = model.Calender_Date.Split('-');
                if (datesplit[0].Length == 1)
                {
                    datesplit[0] = "0" + datesplit[0];
                }
                if (datesplit[1].Length == 1)
                {
                    datesplit[1] = "0" + datesplit[1];
                }
                model.Date = datesplit[0] + datesplit[1] + datesplit[2];
            }
            List<FlightTravCalender> result = FlightReportService.GetFlightDetailsByAgentId_New(model);
            return Json(result);
        }
        #endregion

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult TiceketCopy(string orderid, string transid, string charge, string amount)
        {
            FlightHeaderModel model = new FlightHeaderModel();
            try
            {
                if (!string.IsNullOrEmpty(orderid) && !string.IsNullOrEmpty(charge) && !string.IsNullOrEmpty(amount))
                {
                    LoginSessionDetails lu = new LoginSessionDetails();
                    if (AccountService.GetLoginSession(ref lu))
                        FlightReportService.UpdateTicketTeansCharges(orderid, charge, amount, lu.AgentRegisterSession.UID);
                }
                model.ticketCopy = FlightDataBase.TicketCopyExportPDF(orderid, transid);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }


        #region [Json Secton]
        public JsonResult SendTicketCopy(string email, string OrderId)
        {
            List<string> result = new List<string>();
            if (!string.IsNullOrEmpty(email))
            {
                MailingCredential mailCred = MailingCredentialLibraryHelper.GetMailingDetails(enumMAILING.AIR_PNRSUMMARY.ToString(), "");

                AgencyLoginSession agency = new AgencyLoginSession();
                if (AccountService.IsAgencyLogin(ref agency))
                {
                    if (mailCred != null && mailCred.Counter > 0)
                    {
                        string strBody = FlightDataBase.TicketCopyExportPDF(OrderId, "");

                        bool isMailSent = Mail.SendMail(email, mailCred.MailFrom, mailCred.BCC, mailCred.CC, mailCred.SmtpClient, mailCred.UserID, mailCred.Pass, strBody, "Ticket Copy");
                        if (isMailSent)
                        {
                            TempData["Message"] = "Mail Send Succesfully";
                        }
                        else
                        {
                            TempData["Message"] = "somthing issue..";
                        }
                    }
                }

            }
            return Json(TempData["Message"], JsonRequestBehavior.AllowGet);
        }

        public JsonResult SendInvoiceCopy(string email, string OrderId)
        {
            List<string> result = new List<string>();
            if (!string.IsNullOrEmpty(email))
            {
                MailingCredential mailCred = MailingCredentialLibraryHelper.GetMailingDetails(enumMAILING.AIR_PNRSUMMARY.ToString(), "");

                AgencyLoginSession agency = new AgencyLoginSession();
                if (AccountService.IsAgencyLogin(ref agency))
                {
                    if (mailCred != null && mailCred.Counter > 0)
                    {
                        string strBody = FlightDataBase.InvoiceCopy(OrderId);

                        bool isMailSent = Mail.SendMail(email, mailCred.MailFrom, mailCred.BCC, mailCred.CC, mailCred.SmtpClient, mailCred.UserID, mailCred.Pass, strBody, "Invoice Copy");
                        if (isMailSent)
                        {
                            TempData["Message"] = "Mail Send Succesfully";
                        }
                        else
                        {
                            TempData["Message"] = "somthing issue..";
                        }
                    }
                }

            }
            return Json(TempData["Message"], JsonRequestBehavior.AllowGet);
        }
        public JsonResult InsertOfflineRequest(OfflineRequest offline)
        {
            List<string> result = new List<string>();

            try
            {
                AgencyLoginSession agency = new AgencyLoginSession();
                if (AccountService.IsAgencyLogin(ref agency))
                {
                    offline.AgentId = agency.AgencyId;
                    offline.AgentUser_Id = agency.User_Id;
                    offline.Agentname = agency.Agency_Name;

                    //if (offline.TripType == "oneway")
                    //{
                    if (FlightReportService.InsertProxyDetails(offline))
                    {
                        result.Add("true");
                        result.Add("Proxy Request Sent Successfully.");
                    }
                    //}
                    //else
                    //{
                    //}
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }

        //public JsonResult ReIssueReFund(ReIssueReFund model)
        //{
        //    List<string> result = new List<string>();

        //    try
        //    {
        //        if (!string.IsNullOrEmpty(model.PaxID) && !string.IsNullOrEmpty(model.ActionType))
        //        {
        //            if (model.ActionType == "reissue")
        //            {
        //                string firstStatus = FlightReportService.CheckTktNo(Convert.ToInt32(model.PaxID), model.OrderId, model.PNR);

        //                model = FlightReportService.GetTicketdIntl(Convert.ToInt32(model.PaxID), model.PaxType);

        //                string newpaxid = model.PaxID;

        //                if (!string.IsNullOrEmpty(model.ResuID))
        //                {
        //                    ReIssueReFund Paxds = FlightReportService.OldPaxInfo(model.ResuID, model.Title, model.FName, model.MName, model.LName, model.PaxType);
        //                    //newpaxid = Paxds.PaxId.ToString();
        //                }

        //                string secondStatus = FlightReportService.CheckTktNo(Convert.ToInt32(newpaxid.Trim()), model.OrderId, model.PNR);

        //                if (secondStatus == "0")
        //                {
        //                }
        //                else if (secondStatus == "Reissue request can not be accepted for past departure date." && model.ActionType == "refund")
        //                {
        //                }
        //                else if ((secondStatus == "0" || secondStatus == "Reissue request can not be accepted for past departure date." || secondStatus == "Given ticket number is already ReIssued") || firstStatus == "Refund/Reissue request can not be accepted upto 4 hour prior to departure date. Please contact to airline directly." && model.ActionType == "refund")
        //                {

        //                }
        //                else if (firstStatus == "Refund/Reissue request can not be accepted upto 4 hour prior to departure date. Please contact to airline directly." && model.ActionType == "refund")
        //                {
        //                }
        //                else
        //                {
        //                    //TempData["Message"] = secondStatus;
        //                    result.Add("false");
        //                    result.Add(secondStatus);
        //                }
        //            }
        //            else
        //            {
        //            }

        //            //TempData["type"] = model.ActionType.ToUpper();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //        //clsErrorLog.LogInfo(ex)
        //    }

        //    return Json(result);
        //}
        #endregion

        #region [Export To Excel]        
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public FileResult FlightReportExportToExcel()
        {
            FlightTicketFilter filter = TempData["FlightFilter"] as FlightTicketFilter;           
            TempData.Keep("FlightFilter");
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(FlightReportService.ExportFlightTicketReport(filter));
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Report_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");
                }
            }
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public FileResult RefundReportExportToExcel()
        {
            FlightTicketFilter filter = TempData["RefundFilter"] as FlightTicketFilter;         
            TempData.Keep("RefundFilter");
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(FlightReportService.ExportFlightRefundReport(filter));
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Report_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");
                }
            }
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public FileResult ReissuesReportExportToExcel()
        {
            FlightTicketFilter filter = TempData["ReissuesFilter"] as FlightTicketFilter;
            TempData.Keep("ReissuesFilter");
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(FlightReportService.ExportFlightReIssueReport(filter));
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Report_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");
                }
            }
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public FileResult HoldPnrReportExportToExcel()
        {
            FlightTicketFilter filter = TempData["HoldPnrFilter"] as FlightTicketFilter;
            TempData.Keep("HoldPnrFilter");
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(FlightReportService.ExportFlightHoldPnrReport(filter));
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Report_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");
                }
            }
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public FileResult TicketStatusReportExportToExcel()
        {
            FlightTicketFilter filter = TempData["TicketStatusFilter"] as FlightTicketFilter;
            TempData.Keep("TicketStatusFilter");
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(FlightReportService.ExportFlightTicketStatusReport(filter));
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Report_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");
                }
            }
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public FileResult OfflineRequestReportExportToExcel()
        {
            FlightTicketFilter filter = TempData["OfflineRequestFilter"] as FlightTicketFilter;
            TempData.Keep("OfflineRequestFilter");
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(FlightReportService.OfflineRequestReportExportToExcel(filter));
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Report_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");
                }
            }
        }
        #endregion
    }
}
