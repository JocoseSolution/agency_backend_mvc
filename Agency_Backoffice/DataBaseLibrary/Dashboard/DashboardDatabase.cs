﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DataBaseLibrary.Dashboard
{
    public static class DashboardDatabase
    {
        private static SqlCommand sqlCommand { get; set; }
        private static SqlDataAdapter sqlDataAdapter { get; set; }
        private static DataSet dataSet { get; set; }
        private static DataTable dataTable { get; set; }
        public static List<DataTable> GetDashboardDetail(string fromDate, string toDate, string agencyId, string salesId)
        {
            List<DataTable> objDashList = new List<DataTable>();
            try
            {
                sqlCommand = new SqlCommand("sp_GetNewDashBoardDetails", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@FromDate", (!string.IsNullOrEmpty(fromDate) ? fromDate : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(toDate) ? toDate : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@AgencyId", (!string.IsNullOrEmpty(agencyId) ? agencyId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@SalesExecId", (!string.IsNullOrEmpty(salesId) ? salesId : "")));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "DashBoardDetails");

                if (dataSet != null && dataSet.Tables.Count > 0)
                {
                    objDashList.Add(dataSet.Tables[0]);
                    objDashList.Add(dataSet.Tables[1]);
                    objDashList.Add(dataSet.Tables[2]);
                    objDashList.Add(dataSet.Tables[3]);
                    objDashList.Add(dataSet.Tables[4]);
                    objDashList.Add(dataSet.Tables[5]);
                    objDashList.Add(dataSet.Tables[6]);
                    objDashList.Add(dataSet.Tables[7]);
                    objDashList.Add(dataSet.Tables[8]);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return objDashList;
        }
    }
}
