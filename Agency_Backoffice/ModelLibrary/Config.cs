﻿using System;
using System.Configuration;

namespace ModelLibrary
{
    public static class Config
    {
        public static string MyAmdDBConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["DBConStr"].ConnectionString; }
        }
        public static string ConnectionStrBus
        {
            get { return ConfigurationManager.ConnectionStrings["ConBus"].ConnectionString; }
        }
        public static string ConnectionStrHotel
        {
            get { return ConfigurationManager.ConnectionStrings["ConHotel"].ConnectionString; }
        }
        public static string WebsiteName
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["websitename"]); }
        }
        public static string WebsiteUrl
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["websiteurl"]); }
        }
        public static string WebsiteLogo
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["websitelogo"]); }
        }
        public static string AdminEmail
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["adminemail"]); }
        }
        public static string WebsiteLoginUrl
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["websiteloginurl"]); }
        }
        public static string WebsiteSearchUrl
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["websitesearchurl"]); }
        }
        public static string ApplicationUrl
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["applicationurl"]); }
        }
        public static string CompanyEmailId
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["companymailid"]); }
        }
        public static string CompanyContactNo
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["companycontactno"]); }
        }
    }
}
